const Discord = require('discord.js'),
      colour = new(require('chalk')).constructor({ //Used to make the console have pretty colours
        enabled: true //This isn't normally needed but PM2 doesn't work with chalk unless I do this 
      }),
      fs = require('fs'), //For reading/writing to a file
      reload = require('require-reload')(require);

// load commands
//const commands = require('./commands.js');

const bot = new Discord.Client({autoReconnect:true});

var options = reload('./options/options.json'),
    commandLoader = reload('./utils/commandLoader.js'),
    utils = reload('./utils/utils.js'),
    processCmd = reload('./utils/commandHandler.js'),
    //List of playing status's for the bot to use
    playing = reload('./lists/playing.json');

//Console Log Colours
botC = colour.magenta.bold,
userC = colour.cyan.bold,
guildC = colour.black.bold,
channelC = colour.green.bold,
miscC = colour.blue.bold,
warningC = colour.yellow.bold,
errorC = colour.red.bold;

//commands(bot);


bot.on('ready', () => {
    console.log('I am ready!');

    //console.log(`${botC(client.user.username + ' is now Ready with')} ${errorC(client.shards.size)} ${clientC('Shards.')}`);
    console.log(`Current # of Commands Loaded: ${warningC(Object.keys(commands).length)}`)
    //console.log(`Users: ${userC(bot.users.size)} | Channels: ${channelC(Object.keys(client.channelGuildMap).length)} | Guilds: ${guildC(client.guilds.size)}`)
});

bot.on("message", msg => {
    //If bot isn't ready or if the message author is a client who isn't Kimi do nothing with the message
    //if (!bot.ready || (msg.author.client && msg.author.id !== "174669219659513856")) return;
    //else if (msg.author.id !== '87600987040120832') return; //Used only if I want to disable the bot for everyone but me while testing/debugging
    //else {
        //If used in guild and the guild has a custom prefix set the msgPrefix as such otherwise grab the default prefix
        var msgPrefix = options.prefix;
        //Use Eval on the message if it starts with sudo and used by Mei
        if (msg.content.split(" ")[0] === "sudo" && msg.author.id === "87600987040120832") {
            evalInput(msg, msg.content.split(" ").slice(1).join(' '));
            return;
        }
        //Hot reload all possible files
        if (msg.content.startsWith(options.prefix + 'reload') && msg.author.id === "87600987040120832") reloadModules(msg);
        //If stuff that isn't a command is used in a PM treat it as using cleverbot by adding the correct prefix as well as the 'chat' command text to the message
        if (!msg.channel.guild && !msg.content.startsWith(options.prefix)) msg.content = msgPrefix + "chat " + msg.content;
        //If the message stats with the set prefix
        if (msg.content.startsWith(msgPrefix)) {
            var formatedMsg = msg.content.substring(msgPrefix.length, msg.content.length), //Format message to remove command prefix
                cmdTxt = formatedMsg.split(" ")[0].toLowerCase(), //Get command from the formatted message
                args = formatedMsg.split(' ').slice(1).join(' '); //Get arguments from the formatted message
            if (commandAliases.hasOwnProperty(cmdTxt)) cmdTxt = commandAliases[cmdTxt]; //If the cmdTxt is an alias of the command
            if (cmdTxt === 'channelmute') processCmd(msg, args, commands[cmdTxt], bot); //Override channelCheck if cmd is channelmute to unmute a muted channel
            //Check if a Command was used and runs the corresponding code depending on if it was used in a Guild or not, if in guild checks for muted channel and disabled command
            else if (commands.hasOwnProperty(cmdTxt)) {
                processCmd(msg, args, commands[cmdTxt], bot);
            }
        }
    //}
});

function evalInput(msg, args) {
    var result;
    //Trys to run eval on the text and output either an error or the result if applicable 
    try {
        result = eval("try{" + args + "}catch(err){console.log(err);msg.channel.send(\"```\"+err+\"```\");}");
    } catch (e) {
        console.log(e)
        msg.channel.send("```" + e + "```");
    }
    //If result isn't undefined and it isn't an object return to channel
    if (result && typeof result !== 'object') msg.channel.send(result);
    console.log(result)
}

//Load Commands then Connect(Logs any errors to console and file)
commandLoader.load().then(() => {
    bot.login(options.token).then(console.log(warningC('Connecting with Token')));
}).catch(err => utils.fileLog(err));

function setRandomStatus() {
    bot.shards.forEach(shard => {
        shard.editStatus({
            name: playing[~~(Math.random() * (playing.length))],
            type: 1,
            url: urls[~~(Math.random() * (urls.length))]
        });
    })
}

//Hot Reload ALl Modules
function reloadModules(msg) {
    try {
        utils = reload('./utils/utils.js');
        options = reload('./options/options.json');
        processCmd = reload('./utils/commandHandler.js');
        commandHandler = reload('./utils/commandHandler.js');
        playing = reload('./lists/playing.json');
        commandLoader.load().then(() => {
            console.log(botC('@' + client.user.username + ': ') + errorC('Successfully Reloaded All Modules'));
            msg.channel.send('Successfully Reloaded All Modules').then(message => utils.messageDelete(message))
        });
    } catch (e) {
        console.log(errorC('Error Reloading Modules: ' + e))
    }
}
