const admins = require('./../options/admins.json'); //List of Admin ID's which override permissions check as well as allow use of admin commands

module.exports = (msg, args, cmd, bot) => {
    //Checks for Admin command types
    if (cmd.type === "admin" && !admins.includes(msg.author.id)) return;
    else {
        //Cooldown check, admins ignore cooldowns
        if (!admins.includes(msg.author.id) && cmd.cooldownCheck(msg.author.id))  {
            msg.channel.send(`\`${cmd.name}\` is currently on cooldown for ${cmd.cooldownTime(msg.author.id).toFixed(1)}s`);
        }
        //Process the command
        else {
            //Execute the command
            cmd.exec(msg, args, bot)
            if (msg.channel.guild) {
                //Command Logging in Guilds
                console.log(`${guildC("@" + msg.channel.guild.name + ":")}${channelC(" #" + msg.channel.name)}: ${warningC(cmd.name)} was used by ${userC(msg.author.username)}`);
            }
            //Comand Logging in PM's
            else console.log(`${guildC("@Private Message:")} ${warningC(cmd.name)} was used by ${userC(msg.author.username)}`);
        }
    }
}
