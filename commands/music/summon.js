module.exports = {
    usage: `Summon bot to voice channel`,
    cooldown: 30,
    process: (msg, args, bot) => {
        return new Promise(resolve => {
            //// Only try to join the sender's voice channel if they are in one themselves
            if (msg.member.voiceChannel) {
                msg.member.voiceChannel.join().then(connection => {
                    // Connection is an instance of VoiceConnection
                    msg.reply('I have successfully connected to the channel!');
                }).catch(console.log);
            } else {
                msg.reply('You need to join a voice channel first!');
            }
        });
    }
}
