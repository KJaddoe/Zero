module.exports = {
    usage: `Make bot leave voice channel`,
    process: (msg, args, bot) => {
        return new Promise(resolve => {
            const voiceConnection = bot.voiceConnections.find(val => val.channel.guild.id == msg.guild.id);

            if (voiceConnection === null) return msg.reply('I\'m not in any channel!.');

            voiceConnection.disconnect()
            msg.channel.send(":hear_no_evil:");
        });
    }
}
